#' Basic data transformations for shiny ships app.
#' 
#' The data transformed in this file is saved in shinyships.RData, 
#' so no need to call directly.

ships <- read.csv("ships.csv")

# convert columns to more appropriate types
ships$DATETIME_EPOCH <- as.integer(as.POSIXct(ships$DATETIME))

# Dataframe used for storing the mapping ship_id <-> ship name and type
ship_id_name_df <- ships[, c("SHIP_ID", "SHIPNAME", "ship_type")]
ship_id_name_df <- ship_id_name_df[!duplicated(ship_id_name_df$SHIPNAME), ]